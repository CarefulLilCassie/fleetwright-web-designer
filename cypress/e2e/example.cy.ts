// https://docs.cypress.io/api/introduction/api.html

describe("Server exists", () => {
    it("displays the toolbar", () => {
        cy.visit("/");
        cy.contains(".v-toolbar-title", "FLEETWRIGHT SHIP DESIGNER");
    });
});

describe("VTT Exists", () => {
    it("vtt route works", () => {
        cy.visit("/#/vtt");
        cy.contains("button", "Server");
    });
});