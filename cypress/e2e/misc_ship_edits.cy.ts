function load_sample(partial_sample_name: string){
    cy.get(".sample-dropdown-button").click();
    cy.contains(".v-list-item", partial_sample_name).click();
    cy.contains(".ship-card button", "Edit", {matchCase:false}).last().click();
}

describe("ship editor", () => {
    beforeEach(() => {
        cy.viewport(2000,1000);
        cy.visit("/");
    });
    it("edits ship name, prefix, callsign", () => {
        load_sample("Newsflash");
        cy.get("#ship-name-edit-button").click();
        cy.get("#ship-name-dialog--name").clear().type("asdfasdf");
        cy.contains("#ship-name", "asdfasdf");
        cy.get("#ship-name-dialog--prefix").clear().type("FOO");
        cy.contains("#ship-prefix", "FOO");
        cy.get("#ship-name-dialog--callsign").clear().type("Wolf 1");
        cy.contains("#ship-callsign", "Wolf 1");
        cy.get("#ship-name-dialog--close").click();
    });
    it("print button works", () => {
        load_sample("Newsflash");
        cy.window().then((window) => {
            cy.stub(window, "print").as("print");
        });
        cy.contains("button", "print", {matchCase:false}).click();
        cy.get("@print").should("have.been.called");
    });
    it("adds components", () => {
        load_sample("blank");
        cy.get(".component-widget").scrollIntoView();
        // add one from each group
        cy.get(".component-widget .v-expansion-panel-title").each((group) => {
            cy.wrap(group).click().wait(100);
            cy.get(".v-list-item").first().click().then((card) => {
                const id = card.attr("test-component");
                cy.get(`[data-component="${id}"]`).should("exist");
            });
            cy.wrap(group).click().wait(100);
        });
    });
    it("back button works", () => {
        load_sample("Newsflash");
        cy.get("[aria-label=Back]").click();
        cy.get(".ship-card");
    });
});