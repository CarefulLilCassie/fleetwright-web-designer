# fleetwright-web-designer
Fleetwright Ship Designer written in Vue.

This contains the actual editor code.
Component statistics and such are in [fleetwright-meta](https://gitlab.com/SIGSTACKFAULT/fleetwright-meta)


## Recommended IDE Setup
- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) 
  - Volar Takeover Mode: disable (workspace) `vscode.typescript-language-features` 
- [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).


## Project Setup
```sh
npm install
git submodule init
git submodule update
```

### Compile and Hot-Reload for Development
```sh
npm run dev
```

### Compile and Minify for Production
```sh
npm run build
```


## Contributing
- anything in issues is fair game.
  - make a MR for any issue as you wish
- I've littered the project with comments including `#TODO` and barely any of them are in gitlab
- MRs MUST pass all pipelines
  - ProTip: use `eslint --fix`
  - or the vscode eslint extension's auto-fix command


## Vibe Sheet
- Comp/Con
- ASCBI
- EVE in-game ship editor
  - PyFA
- NEBULOUS
- Factorio (Mod system)
