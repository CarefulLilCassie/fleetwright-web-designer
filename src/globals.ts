import { computed } from "vue";
import { parse } from "yaml";
import { reactive } from "vue";

type int = number; // but just like know in your mind it's an int

export type CrewType = "biological" | "construct" | "infomorph" | "mechanical" | "brainwashed" | "any"
export type ComponentPrototype = {
    name: string,
    type: string,
    density: number,
    durability: number,
    price: number | string,
    fixed_volume?: number,
    modules: {
        // TODO specify them all
        [id: string]: any,
        buff: BuffModuleProto,
    },
}

export type BuffModuleProto = {
    scaling_function?: "volume" | "crew" | 1
    stacking?: boolean,
    add?: any,
    multiply?: any,
    active?: any,
}

export type ChoiceBuffModuleProto = BuffModuleProto[];

export type CombatStationProto = {
    name?: string,
    min_morale: int,
    assigned_crew: {
        min_morale?: int,
        any_crew?: int,
        specific_crew: {
            type: CrewType[],
            amount: int,
        }[],
        buff: BuffModuleProto,
        buff_choice: BuffModuleProto[],
    }[],
}

export type Data = {
    resources?: {
        [id: string]: {
            name: string,
            density?: number,
        }
    },
    components?: {
        [id: string]: ComponentPrototype,
    },
    combat_stations?: {
        [id: string]: CombatStationProto,
    },
    missiles?: {
        guidance: any
        payload: any
        propulsion: any
    },
}

// round to this many decimal places in assorted places
// to avoid having like 0.30000000000000004
export const FLOATING_POINT_ROUNDING = -5;


// new URL("../fleetwright-meta/junctspace.yaml", import.meta.url);
// new URL("../samples.yaml", import.meta.url);

const data_url = import.meta.url.split("/").slice(0, -2).join("/") + "/junctspace.yaml";
const sample_url = import.meta.url.split("/").slice(0, -2).join("/") + "/samples.yaml";

// can't use await due to vue jank
export const data = reactive<Data>({});
export const data_promise = new Promise<void>((resolve, reject) => {
    fetch(data_url).then(
        (response) => {
            if (!response.ok) {
                reject();
                return;
            }
            response.text().then(
                (text) => {
                    const tmp = parse(text);
                    Object.assign(data, tmp);
                    console.log("got data dump", data);
                    resolve();
                }
            );
        }
    );
});

export const samples = reactive<Array<any>>([]);
fetch(sample_url).then(
    (response) => {
        response.text().then(
            (text) => {
                const tmp = parse(text);
                for (const val of Object.values(tmp)) {
                    samples.push(val);
                }
                console.log("got samples", samples);
            }
        );
    }
);

export const type_cache = computed(() => {
    if (Object.keys(data).length == 0) return {};
    const cache: { [type: string]: { [id: string]: ComponentPrototype } } = {
        // force these to be at the top
        engine: {},
        tank: {},
        powerplant: {},
        weapon_direct: {},
        weapon_guided: {},
        ftl_drive: {},
        command: {},
        living_quarter: {},
        store: {},
        recreational: {},
        ewar: {},
        utility: {},
    };
    for (const id in data.components) {
        const component = data.components[id];
        if (cache[component?.type] == undefined)
            cache[component?.type] = {};
        cache[component?.type][id] = component;
    }
    console.log("[type_cache] Done!", cache);
    return cache;
});

export const CREW_LABELS: Map<CrewType, string> = new Map([
    ["biological", "Biological"],
    ["construct", "Construct"],
    ["infomorph", "Infomorph"],
    ["mechanical", "Mechanical"],
    ["brainwashed", "Brainwashed"],
    ["any", "Don't Care"],
]);