import * as winston from "winston";
import { Room } from "colyseus";


const console_format = winston.format.combine(
    winston.format.colorize(),
    winston.format((info) => {
        // hide from format.simple()
        info.timestamp = undefined;
        return info;
    })(),
    winston.format.padLevels(),
    // winston.format.json(),
    winston.format.simple(),
);

winston.add(new winston.transports.Console({
    format: winston.format.combine(
        winston.format.splat(),
        winston.format.timestamp(),
        console_format,
    ),
    level: "info",
}));


export function create_room_logger(room: Room) {
    return winston.createLogger({
        format: winston.format.combine(
            winston.format.splat(),
            winston.format.timestamp(),
            winston.format((info) => {
                info.roomName = room.roomName;
                info.roomId = room.roomId;
                return info;
            })(),
        ),
        transports: [
            new winston.transports.Console({
                format: winston.format.combine(
                    winston.format((info) => {
                        const parts: string[] = [];
                        if (info.roomName && info.roomId) {
                            parts.push(`[${info.roomName}]`);
                        }
                        if (info.clientId) {
                            parts.push(`<${info.clientId}>`);
                        }
                        parts.push(info.message);
                        info.message = parts.join(" ");
                        // don't bother with these because we just added them to the message
                        info.roomName = undefined;
                        info.clientId = undefined;
                        // and i just don't wanna look at these
                        info.roomId = undefined;
                        info.timestamp = undefined;
                        return info;
                    })(),
                    console_format,
                )
            }),
            new winston.transports.File({
                filename: "./vtt-server.log",
                level: "debug",
                format: winston.format.json(),
                maxFiles: 3,
                maxsize: 50000000
            })
        ]
    });
}