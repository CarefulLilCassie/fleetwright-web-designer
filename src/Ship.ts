import { reactive } from "vue";
// model
import type { BuffModuleProto, CrewType } from "./globals";
import { CREW_LABELS, data, data_promise } from "./globals";
import { Component } from "./Component";
import { combat_station_level } from "./simulation";

export class Ship {
    name: string;
    prefix: string;
    callsign: string;
    armour: number;
    author?: string;
    notes?: string;
    components: Component[];
    combat_stations: {
        [id: string]: {
            crew: Record<CrewType, number>,
            data: any,
        }
    };
    _faction_morale: number;

    constructor(name?: string, armour?: number, components?: any, combat_stations?: any) {
        this.name = name || "";
        this.prefix = "";
        this.callsign = "";
        this.armour = armour || 0;
        this.components = components || [];
        this.combat_stations = reactive({});
        data_promise.then(() => {
            for (const id in data.combat_stations) {
                const crew:Record<string,number> = {};
                for(const type of CREW_LABELS.keys()){
                    crew[type] = combat_stations?.[id]?.crew?.[type] ?? combat_stations?.[id]?.[type] ?? 0;
                }
                this.combat_stations[id] = reactive({
                    crew: reactive(crew),
                    data: null
                });
            }
        });
        this._faction_morale = 0; // assumed faction morale
    }

    /**
     * @deprecated
     */
    load_ship(ship: Ship | any) {
        this.name = ship.name;
        this.prefix = ship.prefix;
        this.callsign = ship.callsign;
        this.armour = ship.armour || 0;
        this.author = ship.author || "";
        this.notes = ship.notes;
        this.components = [];
        for (const component of ship.components || []) {
            this.add_component(component);
        }
        for (const id in data.combat_stations) {
            //console.log("loading combat stations", id, ship.combat_stations?.[id], "->", this.combat_stations[id]);
            for(const type of CREW_LABELS.keys()){
                this.combat_stations[id].crew[type] =
                    ship.combat_stations[id]?.[type] ?? // old schema
                        ship.combat_stations?.[id]?.[type] ?? 0;
            }
        }
        console.log("loaded ship", this);
    }

    add_component(component: {id: string, volume: number, armour: boolean, data: any}, index=-1) {
        const target = reactive(
            new Component(component.id, component.volume, component.armour, component.data)
        );
        if(index == -1)
            this.components.push(target);
        else
            this.components.splice(index, 0, target);
    }

    remove_component(target: Component) {
        const index = this.components.findIndex((element) => element._uuid == target._uuid);
        this.components.splice(index, 1);
    }

    *component_modules(): Generator<{component: Component, module_name: string, module_proto:any}, void, undefined> {
        for (const component of this.components) {
            const prototype = component.prototype();
            if (prototype == undefined) {
                continue; // missing prototype
            }
            if (prototype.modules == null) {
                continue; // no modules
            }
            for (const [module_name, module_proto] of Object.entries(prototype.modules)) {
                yield {component, module_name, module_proto};
            }
        }
    }

    *buffs(): Generator<{scaling:{volume?:number, crew?:number}, prototype:BuffModuleProto, non_stacking_id: string}, void, undefined> {
        for(const item of this.component_modules()){
            if(item.module_name == "buff"){
                yield {
                    scaling: {
                        volume: item.component.volume,
                    },
                    prototype: item.module_proto as BuffModuleProto,
                    non_stacking_id: item.component.id,
                };
            }
        }
        if(data.combat_stations == undefined) return;
        for(const [id, proto] of Object.entries(data.combat_stations)){
            const level = combat_station_level(this, proto, this.combat_stations[id].crew);
            if(level==0) continue;
            const crew = Object.values(this.combat_stations[id].crew).reduce((a:number,b:number) => a+b);
            const buff = proto.assigned_crew[level].buff;
            console.log("*buffs: combat stations:", buff, level, crew);
            if(buff)
                yield {
                    scaling: {crew},
                    prototype: buff as BuffModuleProto,
                    non_stacking_id: "combat-station--"+id,
                };
            // TODO handle selections
        }
    }

    /**
     * collect all the buffs which apply to `what_buff`
     */
    calculate_buff(what_buff: string): {add: number, multiply: number} {
        let add = 0;
        let multiply = 1;
        const non_stacking = new Set();
        for (const {scaling, prototype, non_stacking_id} of this.buffs()) {
            const base_add = prototype?.add?.[what_buff] ?? 0;
            const base_mult = prototype?.multiply?.[what_buff] ?? 1;
            if (non_stacking.has(non_stacking_id) && (prototype?.stacking == false)) {
                continue;
            }
            non_stacking.add(non_stacking_id);
            switch (prototype?.scaling_function) {
                case 1: // no scaling
                    add += base_add;
                    multiply *= base_mult;
                    break;
                case undefined:
                case "volume":
                    add += base_add * (scaling.volume??NaN);
                    multiply *= base_mult ** (scaling.volume??NaN);
                    break;
                case "crew":
                    add += base_add * (scaling.crew??NaN);
                    multiply *= base_mult ** (scaling.crew??NaN);
                    break;
                default:
                    console.trace("unknown buff.scaling_function", prototype.scaling_function);
                    break;
            }
        }
        return {
            add: add,
            multiply: multiply
        };
    }

    /**
     * @param base_value starting value for the buff (typically 1 or 0)
     * @param what_buff buff to apply
     */
    apply_buff(base_value: number, what_buff: string): number {
        const buff = this.calculate_buff(what_buff);
        const result = (base_value + buff.add) * buff.multiply;
        console.debug(`apply_buff: ${what_buff} => +${buff.add} *${buff.multiply} =>`, result);
        return result;
    }

    total_volume(): number {
        let sum = 0;
        for (const c of this.components) {
            sum += c.volume;
        }
        return sum;
    }

    /** @returns {int} */
    total_component_mass() {
        let sum = 0;
        for (const c of this.components) {
            sum += c.mass();
            //console.log(`total_component_mass: ${c.volume} -> ${sum}`);
        }
        return sum;
    }

    /** @returns {int} */
    total_cost() {
        let sum = this.armour * 0.2; // armour costs 1/mass
        for (const c of this.components) {
            sum += c.cost();
        }
        return Math.ceil(sum);
    }

    /** @returns {int} */
    total_durability() {
        let sum = 0;
        for (const c of this.components) {
            sum += c.durability();
        }
        return sum;
    }

    /** @returns {int} */
    total_mass() {
        return this.total_component_mass() + this.armour;
    }

    total_module_crew(phase: "strategic" | "tactical") {
        // crew modules
        let sum = 0;
        for (const {component, module_name, module_proto} of this.component_modules()) {
            if (module_name == "crew") {
                sum += Math.ceil(
                    (module_proto?.[phase]?.base || 0)
                    + (module_proto?.[phase]?.per_volume || 0) * component.volume
                );
            }
        }
        return sum;
    }

    total_station_crew(){
        let sum = 0;
        for (const station of Object.values(this.combat_stations)){
            sum += Object.values<number>(station.crew)
                .reduce((prev, current) => prev+current, 0);
        }
        return sum;
    }

    /**
     * total crew the ship needs
     */
    total_crew(): {strategic: number, tactical: number} {
        const sum = {
            strategic: this.total_module_crew("strategic"),
            tactical: this.total_module_crew("tactical"),
        };
        sum.tactical += this.total_station_crew();
        // combat stations
        return sum;
    }

    /**
     * maintenance or combat crew, whichever is higher
     * @returns {int}
     */
    total_crew_worstcase() {
        return Math.max(...Object.values(this.total_crew()));
    }

    /**
     * sum of all quarters with the occupied box ticked
     * @returns {int}
     */
    total_crew_quarters() {
        let sum = 0;
        for (const {component, module_name, module_proto} of this.component_modules()) {
            if (module_name == "quarters") {
                if (component.data?.occupied == true)
                    sum += module_proto.crew * component.volume;
            }
        }
        return sum;
    }

    crew_type_amount(type: CrewType){
        let sum = 0;
        sum += Object.values(this.combat_stations).reduce(
            (prev, current) => current.crew[type] + prev, 0);
        switch(type){
            case "any":
                sum += Math.max(
                    this.total_module_crew("strategic"),
                    this.total_module_crew("tactical"),
                );
        }
        return sum;
    }

    /**
     * calculate the morale for the given crew type
     * @param type which crew type to do the math for
     * @returns the crew type's morale
     */
    crew_type_morale(type: CrewType) {
        let type_morale;
        switch(type){
            case "biological":
            case "construct":
            case "infomorph":
            case "any":
                type_morale = 0;
                if(this.crew_type_amount("brainwashed") > 0)
                    type_morale -= 4;
                // if fleet has any, -=2
                break;
            case "mechanical":
                return 2;
            case "brainwashed":
                return 0;
        }
        return type_morale + this._faction_morale;
    }

    worst_morale(){
        let worst = Infinity;
        for(const type of CREW_LABELS.keys()){
            const type_morale = this.crew_type_morale(type);
            if(type_morale < worst)
                worst = type_morale;
        }
        return worst;
    }

    total_resource_tanks(which: string){
        let sum = 0;
        for (const {component, module_name, module_proto} of this.component_modules()) {
            if (module_name == "tank") {
                if(module_proto.stores == which)
                    sum += component.volume * 10;
            }
        }
        return sum;
    }

    /**
     * sum of all tanks for all resources
     * @deprecated use `total_resource_tanks` instead
     */
    total_tanks() {
        const sum:{[resource:string]: number} = {};
        for (const {component, module_name, module_proto} of this.component_modules()) {
            if (module_name == "tank") {
                const current = sum[module_proto.stores] ?? 0;
                sum[module_proto.stores]= current + component.volume * 10;
            }
        }
        return sum;
    }

    /**
     * total volume of components which are armoured
     */
    total_armoured_volume(): number {
        let sum = 0;
        for (const component of this.components) {
            if (component.armour)
                sum += component.volume;
        }
        return sum;
    }

    /**
     * protection rating of the ship
     */
    protection() {
        return Math.floor(
            120 * (this.armour / Math.pow(this.total_armoured_volume(), 2 / 3))
        );
    }

    /**
     * best jump range of all drives
     */
    best_jump() {
        let best = 0;
        for (const c of this.components) {
            if (c.prototype()?.type == "ftl_drive") {
                const range = c.jump_range(this.total_mass());
                if (range > best)
                    best = range;
            }
        }
        return best;
    }

    /**
     * ship's sensor rating
     * @returns 
     */
    sensors() {
        return this.apply_buff(1, "sensors");
    }
}
