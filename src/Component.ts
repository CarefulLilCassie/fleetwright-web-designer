import { isReactive, toRaw } from "vue";
import { evaluate } from "mathjs";
import { round10 } from "expected-round";
import { v1 as uuidv1 } from "uuid";
// model
import { FLOATING_POINT_ROUNDING, data } from "./globals";
import type { ComponentPrototype } from "./globals";

//////////////////////////////////////////////////////////////////////////////
// COMPONENT
//////////////////////////////////////////////////////////////////////////////

export class Component {
    id: string;
    volume: number;
    armour: boolean;
    data: any;
    _uuid: string;
    _simulation_state: any;

    constructor(id:string, volume = 1, armour = false, data = null) {
        this.id = id;
        this.volume = volume;
        this.armour = armour;
        this.data = data;
        this._uuid = uuidv1();
        this._simulation_state = {};

        // do it this way to make TS happy
        const fixed_volume = this.prototype()?.fixed_volume;
        if (fixed_volume != undefined) {
            this.volume = fixed_volume;
        }
    }

    prototype(): ComponentPrototype | undefined {
        return data.components?.[this.id];
    }

    set_id(new_id: string) {
        this.id = new_id;
    }

    set_volume(new_volume: number) {
        //console.log("set_volume", this.volume, "->", new_volume);
        this.volume = new_volume;
    }

    toggle_armour() {
        this.armour = !this.armour;
    }

    set_data(data: any) {
        this.data = data;
    }

    /** @returns {int} */
    mass() {
        let mass = this.volume * (this.prototype()?.density ?? 1);
        let stored;
        for (const [module_name, module_obj] of Object.entries(this.prototype()?.modules ?? {})) {
            switch (module_name) {
                case "tank":
                    // #TODO handle partially-full tanks
                    stored = module_obj.stores;
                    if (stored == null) {
                        console.warn("no stored for tank", this);
                        break;
                    }
                    mass += (data.resources?.[stored]?.density ?? 1) * this.volume;
                    break;
            }
        }
        return round10(mass, FLOATING_POINT_ROUNDING);
    }

    cost(): number {
        let price: number | string | undefined = this.prototype()?.price;
        if(price === undefined)
            price = "1*volume";
        if(typeof price == "number")
            return price * this.volume;
        if(typeof price == "string")
            return evaluate(price, {volume: this.volume});
        return 1;
    }

    durability(): number {
        return this.volume * (this.prototype()?.durability ?? 1);
    }

    /** @returns {int} */
    crew() {
        return {
            strategic: (this.prototype()?.modules?.crew?.strategic?.base || 0)
                + (this.prototype()?.modules?.crew?.strategic?.per_volume || 0) * this.volume,
            tactical: (this.prototype()?.modules?.crew?.tactical?.base || 0)
                + (this.prototype()?.modules?.crew?.tactical?.per_volume || 0) * this.volume,
        };
    }

    /** 
     * @deprecated move module math to their own file
     */
    jump_range(total_mass: number) {
        const mass_fraction = this.mass() / total_mass;
        const jump_range = this.prototype()?.modules.ftl_drive.jump_range;
        if(typeof jump_range == "string")
            return Math.floor(
                evaluate(jump_range, {v: this.volume, u: mass_fraction})
            );
        const unrounded = (() => {
            switch (jump_range.method) {
                case "log":
                    // js doesn't seem to have log_n so use ln
                    return jump_range.constant + (Math.log(mass_fraction * jump_range.multiplier) / (Math.log(jump_range.base)));
                case "linear":
                    return jump_range.constant + mass_fraction * jump_range.multiplier;
                default:
                    console.warn("unknown ftl_drive.jump_range.method", jump_range.method);
                    return NaN;
            }
        })();
        console.log("jump_range", unrounded);
        return Math.floor(unrounded);
    }

    update_simulation_state(index:string, new_state:any) {
        console.log("update_simulation_state", this, ">>", index, toRaw(new_state));
        try {
            this._simulation_state[index] = new_state;
        } catch (ex) {
            console.log("update_simulation_state failed", ex, isReactive(this._simulation_state));
            return;
        }
        console.log("update_simulation_state success", toRaw(this));
    }

}
