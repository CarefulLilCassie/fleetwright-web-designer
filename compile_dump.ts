import { parse, stringify } from "yaml";
import { readFileSync, writeFileSync } from "fs";
import { glob } from "glob";

export default async function compile() {
    const files = await glob("./fleetwright-meta/**/*.fw");
    const result = {
        components: {},
        resources: {},
        missiles: {},
    };
    for (const file of files) {
        const it = parse(readFileSync(file).toString());
        Object.assign(result.components, it.components, it.component);
        Object.assign(result.resources, it.resources);
        Object.assign(result.missiles, it.missiles, it.missile);
    }
    const yaml_data = stringify(result, { sortMapEntries: true });
    writeFileSync("static/junctspace.yaml", yaml_data);
    console.log(`built static/junctspace.yaml (${Math.round(yaml_data.length / 1024)}kib)`);
}