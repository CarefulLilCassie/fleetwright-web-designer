import ShipNameDialog from "@/components/dialogs/ShipNameDialog.vue";

describe("<ShipNameDialog />", () => {
    const NAME = "#ship-name-dialog--name";
    const PREFIX = "#ship-name-dialog--prefix";
    const CALLSIGN = "#ship-name-dialog--callsign";
    it("renders", () => {
        cy.viewport(1000, 500);
        cy.mount(ShipNameDialog, {
            props: {
                name: "",
                prefix: "",
                callsign: undefined,
                "onUpdate:name": cy.spy().as("name"),
                "onUpdate:prefix": cy.spy().as("prefix"),
                "onUpdate:callsign": cy.spy().as("callsign"),
            }
        }).then(({component}) => {
            const exposed = component?.$?.exposed as any;
            expect(exposed).to.not.equal(undefined);
            cy.then(() => {
                exposed.dialog.value = true;
            });
            cy.get(NAME).type("Hello");
            cy.get(PREFIX).type("ABC");
            cy.get(CALLSIGN).type("World");
            cy.get("@name").should("have.been.calledWith", "Hello");
            cy.get("@prefix").should("have.been.calledWith", "ABC");
            cy.get("@callsign").should("have.been.calledWith", "World");
            cy.get("#ship-name-dialog--close").click();
            cy.then(() => {
                expect(exposed.dialog.value).to.equal(false);
            });
        });
    });
});