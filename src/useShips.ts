import { computed, reactive, watch } from "vue";
import { ship_yaml_export, ship_yaml_import } from "@/serialization";
import type { Ship } from "@/Ship";
import { parse } from "yaml";



const stored_ships = localStorage.getItem("ships") ?? "[]";
const ships = reactive<Ship[]>(
    JSON.parse(stored_ships).map((yaml:string) => ship_yaml_import(parse(yaml)))
);

watch(ships, () => {
    console.log("watch ships");
    const yaml_array = ships.map((ship) => ship_yaml_export(ship));
    localStorage.setItem("ships", JSON.stringify(yaml_array));
});


export function useShips(){
    return ships;
}

export function useShip(index: number){
    return computed<Ship | undefined>({
        get: () => ships[index],
        set: (val) => {
            if(ships[index] == undefined) return false;
            if(val == undefined) return false;
            ships[index] = val;
        }
    });
}