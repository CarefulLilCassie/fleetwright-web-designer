import * as PIXI from "pixi.js";
import { Grid } from "honeycomb-grid";
import { spiral } from "honeycomb-grid";
//vtt
import { draw, layers } from "./renderer";
import { HEX_SIZE } from "./vtt_globals";
import type { ShipObject } from "@server/schema";
import { Tile } from "./Tile";

const font_size = HEX_SIZE * 0.75;
const AXIS_LABELS: [string, { q: number, r: number, s: number }][] = [
    ["+Q", { q: +2, r: -1, s: -1 }],
    ["+R", { q: -1, r: +2, s: -1 }],
    ["+S", { q: -1, r: -1, s: +2 }],
    ["-Q", { q: -2, r: +1, s: +1 }],
    ["-R", { q: +1, r: -2, s: +1 }],
    ["-S", { q: +1, r: +1, s: -2 }],
];

const AXIS_TEXT_CACHE = AXIS_LABELS.map(([label]) => {
    return new PIXI.Text(label, {
        fill: 0xff00ff,
        fontSize: font_size,
        fontWeight: "bold",
        fontFamily: "'Atkinson Hyperlegible'",
    });
});

export class MyGrid extends Grid<Tile> {
    previous_hover: Tile | null = null;
    world_size: number;
    radius: number;
    selected_ship: ShipObject | undefined;

    constructor(radius: number) {
        super(Tile, spiral({ radius }));
        this.radius = radius;
        this.world_size = HEX_SIZE * (radius + 1) * 5;
    }
    /**
     * draw Q, R, S outside the grid
     * @param wrapper origin at grid center
     */
    draw_axes(): PIXI.Container {
        const container = new PIXI.Container();
        container.parentLayer = layers.brackets;
        for (const i in AXIS_LABELS) {
            const [, vector] = AXIS_LABELS[i];
            const text = AXIS_TEXT_CACHE[i];
            const hex = this.getHex(vector);
            if (!hex) {
                // console.warn("draw_axes: couldn't get hex!");
                break;
            }
            let x = (hex.x);
            let y = (hex.y);
            x *= (this.radius + 1.5) / 2;
            y *= (this.radius + 1.5) / 2;
            // x -= font_size / 2; // hard-coded offset to roughly center
            // y -= font_size / 2;
            text.anchor.set(0.5);
            text.x = x;
            text.y = y;
            container.addChild(text);
        }
        return container;
    }

    draw_border(viewport: PIXI.Container) {
        // let box = (viewport.plugins.plugins["clamp"] as Clamp).options;
        // console.log("border", box);
        const line = viewport.addChild(new PIXI.Graphics());
        line.lineStyle(10, 0xff0000).drawRect(0, 0, this.world_size, this.world_size);
    }

    hex_hover_handler(x: number, y: number) {
        const hex = this.pointToHex({ x, y }, { allowOutside: false }) ?? null;
        if (hex == undefined) {
            // outside the grid
            if (this.previous_hover != null) {
                console.debug(`hover: ${this.previous_hover?.toString()} -> outside`);
                this.previous_hover.onmouseleave();
                this.previous_hover = null;
                draw();
            }
        } else {
            if (this.previous_hover != hex) {
                const prev_string = this.previous_hover?.toString() ?? "outside";
                console.debug(`hover: ${prev_string} -> ${hex.toString()}`);
                this.previous_hover?.onmouseleave();
                this.previous_hover = hex;
                hex.onmouseenter();
                draw();
            }
        }
    }
}