import type { Component } from "./Component";
import type { MathExpression } from "mathjs";
import { evaluate } from "mathjs";
import { round10 } from "expected-round";
import { toRaw } from "vue";
//model
import type { CombatStationProto, CrewType } from "./globals";
import type { Ship } from "./Ship";

type SimState = any; // #TODO

/**
 * combines the inputs/outputs of a converter, or the fuel/alternator of an engine, or etc.
 * to the resources object that the simulation expects
 * @param {Object.<string,int>} positives 
 * @param {Object.<string,int>} negatives 
 */
export function collect_resources(
    positives: Record<string, MathExpression> | undefined,
    negatives: Record<string, MathExpression> | undefined,
    volume: number
) {
    positives = positives ?? {};
    negatives = negatives ?? {};
    const resources:Record<string, number> = {};
    for (const [resource, expression] of Object.entries(positives)) {
        resources[resource] = (resources[resource] ?? 0)
            + evaluate(expression, {v: volume});
    }
    for (const [resource, expression] of Object.entries(negatives)) {
        resources[resource] = (resources[resource] ?? 0)
            - evaluate(expression, {v: volume});
    }
    for(const resource of Object.keys(resources)){
        // round to 2 places
        const rounded = round10(resources[resource], -2);
        resources[resource] = rounded;
    }
    return resources;
}

/**
 * @param {Ship} ship
 * @param {str} which_resource
 * @returns {int} 
 */
export function simulate_resource_delta(ship: Ship, which_resource: string) {
    let delta = 0;
    for (const component of ship.components) {
        for (const simulated of Object.values<SimState>(component._simulation_state)) {
            delta += simulated?.resources?.[which_resource] ?? 0;
        }
    }
    if (isNaN(delta))
        console.trace("simulate_resource_delta: result is NaN!", toRaw(ship), which_resource);
    return delta;
}

/**
 * @param {Ship} ship
 * @returns {double} 
 */
export function simulate_thrust(ship: Ship) {
    let sum = 0;
    for (const component of ship.components) {
        for (const simulated of Object.values<SimState>(component._simulation_state || {})) {
            sum += simulated?.thrust ?? 0;
        }
    }
    return sum;
}

/**
 * @param {Ship} ship
 * @returns {double}
 */
export function simulate_acceleration(ship: Ship) {
    const base = simulate_thrust(ship) / ship.total_mass();
    if (base < 1) {
        // you don't have any engines
        // there's nothing to buff
        return 0;
    }
    return ship.apply_buff(base, "acceleration");
}

/**
 * @param {Ship} ship
 * @returns {double}
 */
export function simulate_ecm(ship: Ship) {
    const heat = 0; // simulate_resource_delta(ship, "HEAT");
    const volume = ship.total_volume();
    const raw_ecm = ship.apply_buff(0, "raw_ecm");
    const ecm = (1500 * raw_ecm) / (volume + heat);
    const buffed_ecm = ship.apply_buff(ecm, "ecm");
    console.log(`simulate_ecm: (heat=${heat}, volume=${volume}, raw_ecm=${raw_ecm}) => ${ecm} => ${buffed_ecm}`);
    return buffed_ecm;
}

/**
 * Ship's total morale
 * including faction morale
 * @param {Ship} ship 
 * @returns {int}
 */
export function simulate_morale(ship: Ship) {
    return ship._faction_morale;
}

/**
 * Ship's total Delta-V
 * @param {Ship} ship 
 * @returns {int}
 */
export function simulate_dv(ship: Ship) {
    const engines = ship.components.filter((component: Component) => component.prototype()?.type == "engine");
    const first_engine = engines[0];
    console.log(`simulate_dv: engines=${engines}`);
    if(first_engine == undefined) return null;
    const module = first_engine.prototype()?.modules.engine;
    const [fuel, fuel_flow] = Object.entries<number>(module.fuel)[0];
    const stored_fuel = ship.total_tanks()[fuel] ?? 0;
    console.log(`simulate_dv: (stored_fuel=${stored_fuel}, flow=${fuel_flow})`);
    const burn_time = stored_fuel/(fuel_flow*first_engine.volume);
    const acceleration = (module.thrust * first_engine.volume) / ship.total_mass();
    const result = burn_time * acceleration;
    console.log(`simulate_dv: (burn=${burn_time}, accel=${acceleration}) => ${result}`);
    return Math.floor(result);
}

export function combat_station_level(
    ship: Ship,
    prototype: CombatStationProto| undefined,
    crew: Record<CrewType, number>
){
    const sum_crew = Object.values(crew).reduce((prev, current) => prev + current, 0);
    if (prototype?.assigned_crew == undefined){
        return -1;
    }
    let level = 0;
    for (const requirements of prototype.assigned_crew) {
        if (requirements.min_morale) {
            if(simulate_morale(ship) < requirements.min_morale)
                break;
        }
        if (requirements.any_crew) {
            if (requirements.any_crew <= sum_crew) {
                level++;
                continue;
            }
        } else if (requirements.specific_crew) {
            let outer_pass = true;
            for (const specific_def of requirements.specific_crew) {
                let inner_pass = false;
                let type: CrewType;
                for (type of specific_def.type) {
                    if (crew[type] >= specific_def.amount) {
                        inner_pass = true;
                        break;
                    }
                }
                if (!inner_pass) outer_pass = false;
            }
            if (outer_pass) {
                level++;
                continue;
            }
        }
        // if we get here without hitting a continue
        // we must have failed the check
        break;
    }
    return level;
}