import * as PIXI from "pixi.js";
import type { HexCoordinates, PartialCubeCoordinates } from "honeycomb-grid";
import { Orientation } from "honeycomb-grid";
import { defineHex } from "honeycomb-grid";
import { spiral } from "honeycomb-grid";
//vtt
import { HEX_SIZE, grid, room } from "./vtt_globals";
import { HexVector, type ShipObject } from "@server/schema";
import { draw_arrow, layers, draw as redraw } from "./renderer";
import type { MyGrid } from "./MyGrid";


/**
 * Client-side tile object
 */
export class Tile extends defineHex({
    dimensions: HEX_SIZE,
    orientation: Orientation.POINTY,
}) {
    hi: boolean = false;
    hover: boolean = false;
    hi_source: ShipObject | undefined;
    graphics: PIXI.Graphics = new PIXI.Graphics();
    qrs_text: PIXI.Text;
    _cached_temp_burn?: PartialCubeCoordinates;

    constructor(coordinates?: HexCoordinates) {
        super(coordinates);
        this.qrs_text = new PIXI.Text(`${this.q},${this.r},${this.s}`, {
            fill: 0x00ffff, fontSize: this.height / 8, fontFamily: "'Atkinson Hyperlegible'", fontStyle: "italic"
        });
        this.qrs_text.setTransform(this.corners[3].x + this.height/80, this.corners[3].y - this.height / 8);
        this.qrs_text.parentLayer = layers.coordinates;
    }

    onmouseenter() {
        this.hover = true;
        const g = grid.value;
        if (g?.selected_ship) {
            if (this.hi) {
                this._cached_temp_burn = {
                    q: this.q - g.selected_ship.q - g.selected_ship.velocity.q,
                    r: this.r - g.selected_ship.r - g.selected_ship.velocity.r,
                };
                g.selected_ship.temp_burn = new HexVector(this._cached_temp_burn.q, this._cached_temp_burn.r);
            } else {
                g.selected_ship.temp_burn = undefined;
            }
        }
    }

    onmouseleave() {
        this.hover = false;
    }

    click_handler(grid: MyGrid) {
        // is there a ship on this hex?
        // TODO more efficient way to do this
        let ship: ShipObject | undefined;
        for (const s of room.value?.state?.ships?.values() ?? []) {
            if ((this.q == s.q) && (this.r == s.r)) {
                ship = s;
                break;
            }
        }
        console.log(this.toString(), "clicked", ship);

        if (this.hi) {
            if (grid.selected_ship) {
                console.log("change burn", grid.selected_ship.uuid, this._cached_temp_burn);
                room.value?.send("change_burn", {
                    uuid: grid.selected_ship.uuid,
                    vector: this._cached_temp_burn,
                });
                grid.selected_ship.temp_burn = undefined;
            }
            grid.forEach(h => { h.hi = false; });
        } else {
            grid.forEach(h => { h.hi = false; });
            if (ship) {
                for (const hex of grid.traverse(
                    spiral({
                        radius: ship.accel(), start: {
                            q: ship.q + ship.velocity.q,
                            r: ship.r + ship.velocity.r,
                        }
                    })
                )) {
                    hex.hi = true;
                    grid.selected_ship = ship;
                }
                // this.onmouseenter();
            }
        }
        redraw();
    }

    border(): PIXI.ColorSource {
        return "#555";
    }

    fill(): PIXI.ColorSource {
        const result = { h: 0, s: 0, v: 15 };
        if (this.hi)
            result.v += 10;
        if (this.hover)
            result.v += 10;
        return result;
    }

    draw(grid: MyGrid): PIXI.Container {
        this.graphics.clear();
        this.graphics.removeChildren();
        const polygon = new PIXI.Polygon(this.corners);
        this.graphics.lineStyle(HEX_SIZE / 20, this.border());
        this.graphics.beginFill(this.fill());
        this.graphics.drawShape(polygon);

        if(this.hover){
            this.graphics.addChild(this.qrs_text);
        }
        this.graphics.interactive = true;
        this.graphics.onclick = () => { this.click_handler(grid); };

        return this.graphics;
    }

    toString() {
        return `Tile(${this.q}, ${this.r})`;
    }
}

export function draw_ship(grid: MyGrid, ship: ShipObject) {
    const graphics = new PIXI.Graphics();
    const tile = grid.getHex(ship);
    if (tile) {
        graphics.x = tile.x;
        graphics.y = tile.y;
        graphics.beginFill(0xff00ff);
        graphics.drawCircle(0, 0, HEX_SIZE / 3);
        graphics.addChild(draw_ship_vectors(grid, ship));
        if(tile.hover)
            graphics.addChild(draw_ship_bracket(ship));
        graphics.interactive = true;
        graphics.onclick = () => { tile.click_handler(grid); };
    }
    return graphics;
}

export function draw_ship_bracket(ship: ShipObject): PIXI.Container {
    const graphics = new PIXI.Graphics();
    graphics.parentLayer = layers.brackets;
    graphics.beginFill("#fff", 0.5);
    graphics.x += HEX_SIZE / 3;
    // graphics.y -= HEX_SIZE*(3/4);
    graphics.drawRect(0, 0, HEX_SIZE * 4, HEX_SIZE*1.5);
    const parts: string[] = [];
    const dv = grid.value?.distance({ q: 0, r: 0 }, ship.burn);
    parts.push(ship.hydrate().name);
    parts.push(`POS: (${ship.q}, ${ship.r})`);
    parts.push(`VEL: (${ship.velocity.q}, ${ship.velocity.r})`);
    parts.push(`BRN: (${ship.burn.q}, ${ship.burn.r}) = ${dv}\u0394v`);
    parts.push(`Accel: ${ship.accel()}`);
    const text = new PIXI.Text(parts.join("\n"), {
        fontFamily: "Atkinson Hyperlegible",
        fontSize: HEX_SIZE / 4,
    });
    graphics.addChild(text);
    return graphics;
}

/**
 * draw the ship's velocity and burn vectors.
 * assumes we're in ship-space
 */
export function draw_ship_vectors(grid: MyGrid, ship: ShipObject): PIXI.Container {
    const container = new PIXI.Container();
    if (ship.velocity.isnonzero()) {
        container.addChild(
            draw_arrow(grid, { q: 0, r: 0 }, ship.velocity, {
                fill_tip: true,
            })
        );
    }
    if (ship.temp_burn?.isnonzero()) {
        container.addChild(
            draw_arrow(grid, ship.velocity, {
                q: ship.velocity.q + ship.temp_burn.q,
                r: ship.velocity.r + ship.temp_burn.r,
            }, {
                fill_tip: false,
            })
        );
    } else if (ship.burn.isnonzero()) {
        container.addChild(
            draw_arrow(grid, ship.velocity, {
                q: ship.velocity.q + ship.burn.q,
                r: ship.velocity.r + ship.burn.r,
            }, {
                fill_tip: false,
            })
        );
    }
    return container;
}