import { Schema, type } from "@colyseus/schema";
import { MapSchema } from "@colyseus/schema";
import { parse } from "yaml";
import { v4 as uuidv4 } from "uuid";
import { Client } from "colyseus";

export class HexVector extends Schema {
    @type("int64") q: number;
    @type("int64") r: number;
    constructor(q: number = 0, r: number = 0) {
        super();
        this.q = q;
        this.r = r;
    }
    iszero() {
        return (this.q == 0) && (this.r == 0);
    }
    isnonzero() {
        return !this.iszero();
    }
}

// https://docs.colyseus.io/colyseus/state/schema/
export class ShipObject extends Schema {
    constructor(yaml, q, r, dq=0, dr=0) {
        super();
        this.uuid = uuidv4();
        this.ship_yaml = yaml;
        this.q = q;
        this.r = r;
        this.velocity = new HexVector(dq, dr);
        this.burn = new HexVector(0, 0);
    }
    _cached_hydrated: any | undefined;
    @type("string") uuid: string;
    @type("string") ship_yaml: string;
    @type("int64") q: number;
    @type("int64") r: number;
    @type(HexVector) velocity: HexVector; // current velocity
    @type(HexVector) burn: HexVector; // upcoming DV
    temp_burn?: HexVector; // used by client while editing burn

    hydrate() {
        if (this._cached_hydrated == undefined) {
            this._cached_hydrated = parse(this.ship_yaml);
        }
        return this._cached_hydrated;
    }

    accel(){
        // this.hydrate() ...
        return 4;
    }
}

export class Player extends Schema {
    @type("string") id: string;
    @type("boolean") ready: boolean;
}

export class MySchema extends Schema {
    constructor() {
        super();
        this.grid_radius = 10;
        this.ships = new MapSchema();
        this.players = new MapSchema();
    }
    @type("int32") grid_radius: number = 10;
    @type({ map: ShipObject }) ships: MapSchema<ShipObject>;
    @type({ map: Player }) players: MapSchema<Player>;
}