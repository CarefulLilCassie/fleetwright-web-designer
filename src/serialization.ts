import { CREW_LABELS } from "./globals";
import { Document } from "yaml";
import { unref } from "vue";
//model
import { Component } from "./Component";
import type { CrewType } from "./globals";
import { Ship } from "./Ship";

export function ship_yaml_export(ship: Ship): string {
    const root = new Document({
        type: "ship",
        name: ship.name ?? "",
        prefix: ship.prefix || undefined,
        callsign: ship.callsign || undefined,
        armour: ship.armour || 0,
        author: ship.author || undefined,
        notes: "",
        components: [],
        combat_stations: {},
    });

    if(ship.notes){
        const notes = root.createNode(ship.notes);
        if("value" in notes) // makes TS happy
            notes.type = "BLOCK_LITERAL";
        root.set("notes", notes);
    }

    for (const c of ship.components) {
        // avoid exporting internal stuff
        /** @type {YAMLMap} */
        const tmp = root.createNode({
            id: c.id,
            //type: c.prototype().type,
            volume: c.volume,
            armour: c.armour,
            data: c.data == null ? undefined : c.data
        });
        tmp.flow = true;
        tmp.commentBefore = ` ${c.prototype()?.name ?? c.id} | ${c.mass()}M | $${c.cost()}`;
        root.addIn(["components"], tmp);
    }

    for (const station_id of Object.keys(ship.combat_stations)) {
        // hide stations and assignments with no crew
        const assignments: Partial<Record<CrewType, number | undefined>> = {};
        for (const crew_type of CREW_LABELS.keys()) {
            const amount = unref(ship.combat_stations[station_id].crew[crew_type]);
            if (amount)
                assignments[crew_type] = amount;
        }
        const total = Object.values(assignments).reduce((prev, current) => prev + current, 0);
        const node = root.createNode(assignments);
        if (Object.values(assignments).length > 1)
            node.comment = ` (total ${total})`;
        if (total > 0)
            root.addIn(["combat_stations", station_id], {
                crew: node,
                data: ship.combat_stations[station_id].data
            });
    }
    return root.toString({
        lineWidth: -1,
        minContentWidth: -1,
        indentSeq: false,
        flowCollectionPadding: false,
        defaultKeyType: "PLAIN",
        defaultStringType: "QUOTE_DOUBLE",
    });
}

export function ship_yaml_import(parsed: any): Ship {
    const result = new Ship(
        parsed.name,
        parsed.armour,
        parsed.components.map((element:any) =>
            new Component(
                element.id, element.volume,
                element.armour, element.data,
            )
        ),
        parsed.combat_stations,
    );
    result.notes = parsed.notes;
    return result;
}