import { createRouter, createWebHashHistory } from "vue-router";
import EditListView from "@/views/EditListView.vue";
import NotFound from "@/views/NotFound.vue";
import ShipView from "@/views/ShipView.vue";
import VTTView from "@/views/VTTView.vue";

const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: "/",
            name: "home",
            component: EditListView,
            alias: ["/ship/", "/fleet/"],
            meta: { back: false },
        },
        {
            path: "/vtt/",
            name: "vtt",
            component: VTTView,
            meta: { back: false },
            props: true,
        },
        {
            path: "/vtt/server=:endpoint(.*)",
            name: "vtt_permalink",
            component: VTTView,
            meta: { back: false },
            props: true,
        },
        {
            path: "/ship/:index",
            props: true,
            name: "ship",
            component: ShipView,
            meta: { back: true },
        },
        {
            path: "/:pathMatch(.*)*",
            name: "NotFound",
            component: NotFound,
            meta: { back: false },
        },
    ]
});

export default router;
