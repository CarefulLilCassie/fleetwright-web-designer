import { defineConfig } from "cypress";

export default defineConfig({
    e2e: {
        video: false,
        specPattern: "cypress/e2e/**/*.cy.ts",
        baseUrl: "http://localhost:5173"
    },
    component: {
        video: false,
        specPattern: "cypress/__tests__/**/*.cy.ts",
        devServer: {
            framework: "vue",
            bundler: "vite",
        },
    }
});
