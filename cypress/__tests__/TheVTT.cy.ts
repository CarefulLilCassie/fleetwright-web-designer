import { Hex } from "honeycomb-grid";
import VTT from "@/views/VTTView.vue";

describe("<TheVTT />", () => {
    const ITERATIONS = 3;
    it("doesn't leak memory", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(VTT).then(({ component }) => {
            cy.viewport(1000,500);
            cy.get("#server-url").clear().type("ws://localhost:9118");
            cy.contains("button.bg-primary", "connect", { matchCase: false }).click();
            cy.wait(1000);
            cy.then(() => {
                const grid = component.$.exposed?.grid?.value;
                expect(grid).to.not.equal(undefined);
                
                for(let i=0;i<ITERATIONS;i++){
                    cy.then(() => {
                        grid.forEach((tile:Hex) => {
                            cy.then(() => {
                                grid.hex_hover_handler(tile.x, tile.y);
                            });
                            cy.wait(5);
                            cy.then(() => {
                                grid.hex_hover_handler(grid.world_size, grid.world_size);
                            });
                        });
                    });
                }
            });
        });
    });
});