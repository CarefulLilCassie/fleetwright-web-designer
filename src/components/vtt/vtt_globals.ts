import type { Client, Room } from "colyseus.js";
import { Ref, ref, shallowRef } from "vue";
//vtt
import { MyGrid } from "./MyGrid";
import type { MySchema } from "vtt-server/schema";

export const HEX_SIZE = 1000;

export const grid: Ref<MyGrid | undefined> = shallowRef();
export const client: Ref<Client | undefined> = shallowRef();
export const room: Ref<Room<MySchema> | undefined> = ref();
export const state: Ref<any> = ref();