import TheToolbar from "@/components/TheToolbar.vue";

describe("<TheToolbar />", () => {
    it("renders", () => {
        // see: https://on.cypress.io/mounting-vue
        cy.mount(TheToolbar, {
            props: {
                debug: false,
                theme: "dark",
            },
        });
    });
});