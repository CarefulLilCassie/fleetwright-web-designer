import MissileEditor from "@/components/ship/modules/MissileEditor.vue";
import { data } from "@/globals";

function props(size: "light" | "heavy" = "light") {
    return {
        guidance: "command_guidance",
        payload: "kinetic",
        propulsion: "none",
        count: 2,
        size: size,
        max_missiles: 5,
        "onUpdate:guidance": cy.spy().as("guidance"),
        "onUpdate:payload": cy.spy().as("payload"),
        "onUpdate:propulsion": cy.spy().as("propulsion"),
        "onUpdate:count": cy.spy().as("count"),
        "onDelete": cy.spy().as("delete")
    };
}

function check_matches_data() {
    for (const id of ["guidance", "payload", "propulsion"]) {
        cy.get(`[cy-id=missile-edit--${id}]`).parent().click().wait(200);
        cy.get(".v-overlay.v-menu .v-list-item").each((el, i) => {
            cy.wrap(el).click();
            cy.get(`@${id}`).should("have.been.calledWith", Object.keys(data.missiles?.[id])[i]);
        });
    }
}

describe("<MissileEditor />", () => {
    beforeEach(() => {
        cy.viewport(1000, 500);
    });
    it("delete button works", () => {
        cy.mount(MissileEditor, {
            props: props(),
        });
        cy.get("[cy-id=missile-edit--delete]").click();
        cy.get("@delete").should("have.been.calledOnce");
    });
    it("count dropdown works", () => {
        cy.mount(MissileEditor, {
            props: props("light")
        });
        cy.get("[cy-id=missile-edit--count]").parent().click();
        cy.get(".v-overlay.v-menu .v-list-item").last().click();
        cy.get("@count").should("have.been.calledWith", 5);
    });
    it("works with heavy missiles", () => {
        cy.mount(MissileEditor, {
            props: props("heavy")
        }).then(({ component }) => {
            // #TODO don't hard-coded these
            expect(component.$.exposed?.single_price.value).to.eq(1);
            expect(component.$.exposed?.price.value).to.eq(2);
        });
        check_matches_data();
    });
    it("works with light missiles", () => {
        cy.mount(MissileEditor, {
            props: props("light")
        }).then(({ component }) => {
            expect(component.$.exposed?.single_price.value).to.eq(0.1);
            expect(component.$.exposed?.price.value).to.eq(0.2);
        });
        check_matches_data();
    });
});