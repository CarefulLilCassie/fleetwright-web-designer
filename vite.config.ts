/* eslint-disable linebreak-style */
/* eslint-disable quotes */
import { URL, fileURLToPath } from "node:url";
import { execSync } from "node:child_process";

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

import compile from "./compile_dump";

const web_describe = execSync("git describe --always --dirty").toString().trim();
const meta_describe = execSync("git -C ./fleetwright-meta describe --always --dirty").toString().trim();

compile();
// can't use top-level await due to Reasons
// will be done by the time vite copies the static files

// https://vitejs.dev/config/
export default defineConfig(() => {
    return {
        build: {
            outDir: "public", // gitlab pages likes it this way
            sourcemap: true,
        },
        plugins: [
            vue(),
        ],
        resolve: {
            alias: {
                "@": fileURLToPath(new URL("./src", import.meta.url)),
                "@server": fileURLToPath(new URL("./vtt-server", import.meta.url)),
            }
        },
        base: "./", // https://stackoverflow.com/a/71536031
        publicDir: "static",
        define: {
            __WEB_GIT_DESCRIBE__: JSON.stringify(web_describe),
            __META_GIT_DESCRIBE__: JSON.stringify(meta_describe),
        },
    };
});
