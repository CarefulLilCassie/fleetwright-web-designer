import ComponentAddWidget from "@/components/ComponentAddWidget.vue";

describe("<ComponentAddWidget />", () => {
    it("has at least one item per category", () => {
        cy.viewport(500,1000);
        const select = cy.spy().as("select");
        cy.mount(ComponentAddWidget, { props: {
            "onSelect": select
        }});
        cy.get("button.v-expansion-panel-title").each((element) => {
            cy.wrap(element).scrollIntoView().click().wait(10); // for animation to finish
            cy.get(".v-list-item");
            cy.wrap(element).scrollIntoView().click();
        });
    });
    it("emits the @select event", () => {
        cy.viewport(500,1000);
        const select = cy.spy().as("select");
        cy.mount(ComponentAddWidget, { props: {
            "onSelect": select
        }});
        cy.get("button.v-expansion-panel-title").each((group) => {
            cy.wrap(group).scrollIntoView().click().wait(50); // for animation to finish
            cy.get(".v-list-item").each((item) => {
                cy.wrap(item).click();
                cy.get("@select").should("have.been.calledOnceWith", item.attr("test-component"));
                cy.then(() => {
                    select.resetHistory();
                });
            });
            cy.wrap(group).click().wait(10); // for animation to finish
        });
    });
});