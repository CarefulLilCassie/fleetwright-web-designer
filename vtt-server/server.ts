/* eslint-disable snakecasejs/snakecasejs */
import * as winston from "winston";
import { Room, Server } from "colyseus";
import type { Client } from "colyseus";
// import { MapSchema, Schema, type } from "@colyseus/schema";
import { Grid, Hex, Orientation, defineHex, line, spiral } from "honeycomb-grid";
import { HexVector, MySchema, Player, ShipObject } from "./schema";
import { create_room_logger } from "./logging";


const server = new Server();
class GameRoom extends Room<MySchema> {
    patchRate = 1000;
    autoDispose = false;
    logger: winston.Logger;
    grid: Grid<Hex>;

    onCreate(options: any) {
        this.logger = create_room_logger(this);
        this.logger.info("room created", { options });
        this.setState(new MySchema());
        this.grid = new Grid( // TODO this in the state
            defineHex({ orientation: Orientation.POINTY }),
            spiral({ radius: this.state.grid_radius })
        );

        this.onMessage("*", (client, type, message) => {
            this.log_client_event(client, `*${type}`, message, "warn");
        });
        this.onMessage("chat", (client, message) => {
            this.log_client_event(client, "chat", message);
            this.broadcast("chat", { author: client.id, content: String(message) });
        });
        this.onMessage("add_ship", (client, message) => {
            const uuid = this.add_ship(message.yaml, message.q, message.r);
            this.log_client_event(client, "add_ship", { q: message.q, r: message.r, uuid: uuid });
        });
        this.onMessage("change_burn", (client, message) => {
            this.log_client_event(client, "change_burn", message);
            this.state.ships.get(message.uuid).burn = new HexVector(
                message.vector.q, message.vector.r
            );
            this.broadcastPatch();
        });
        this.onMessage("ready_up", (client, message) => this.ready_up(client, message));
    }

    async ready_up(client, { state }: { state: boolean }) {
        this.state.players.get(client.id).ready = state;
        // check if all players are ready
        this.log_client_event(client, "ready_up", { state });
        if (this.are_all_players_ready()) {
            this.state.players.forEach((p) => p.ready = false);
            this.step();
        }
    }

    are_all_players_ready(): boolean{
        const ready_count = Array.from(this.state.players.values()).reduce<number>((prev, current) => prev + (current.ready ? 1 : 0), 0);
        const player_count = this.state.players.size;
        if(ready_count == player_count){
            this.logger.info("all players ready");
            return true;
        } else {
            return false;
        }
    }

    step() {
        this.logger.info("step");
        this.apply_burn();
        this.apply_velocity();
        this.broadcast("step");
        this.broadcastPatch();
    }

    /**
     * adds the burn of all ships to their velocity
     */
    apply_burn() {
        this.state.ships.forEach(ship => {
            ship.velocity.q += ship.burn.q;
            ship.velocity.r += ship.burn.r;
            ship.burn = new HexVector(0, 0);
        });
    }

    /**
     * moves all ships by their velocity
     */
    apply_velocity() {
        this.state.ships.forEach(ship => {
            ship.q += ship.velocity.q;
            ship.r += ship.velocity.r;
            if (this.grid.getHex(ship) == undefined) {
                // outside the grid
                // draw a line towards the origin and get the first one that's actually in the grid
                const clamped = this.grid.traverse(line({
                    start: ship, stop: { q: 0, r: 0 }
                })).toArray()[0];
                this.logger.info("ship moved outside grid; clamping position and resetting velocity.", { uuid: ship.uuid, clamped });
                ship.q = clamped.q;
                ship.r = clamped.r;
                ship.velocity.q = ship.velocity.r = 0;
            }
        });
    }

    add_ship(yaml, q, r) {
        const wrapper = new ShipObject(yaml, q, r, 0, 0);
        this.state.ships.set(
            wrapper.uuid, wrapper
        );
        return wrapper.uuid;
    }

    onJoin(client: Client, options) {
        this.log_client_event(client, "joined", { options });
        this.broadcast("chat", { author: "system", content: `*${client.id} joined*` });
        const player = new Player();
        player.id = client.id;
        player.ready = false;
        this.state.players.set(client.id, player);
    }
    onLeave(client: Client, consented?: boolean) {
        this.log_client_event(client, "left", { consented });
        this.state.players.delete(client.id);
        this.broadcast("chat", { author: "system", content: `*${client.id} left*` });
    }

    /**
     * standardized log incoming message
     */
    log_client_event(client: Client, message: string, meta: any = {}, level: string = "info") {
        this.logger.log(level, message, {
            clientId: client.id,
            ...meta
        });
    }
}


const port = parseInt(process.env.PORT ?? "9119");
server.define("main", GameRoom);
server.listen(port, undefined, undefined, () => {
    winston.info("server up", { port });
});
