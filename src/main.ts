import App from "@/App.vue";
import VueSortable from "vue3-sortablejs"; // #TODO figure out how to make TS not complain about this
import { createApp } from "vue";

import VuetifyNotifier from "vuetify-notifier";
import router from "./plugins/router";
import { vuetify } from "./plugins/vuetify";

// icons
import "@mdi/font/css/materialdesignicons.css";
import "@fortawesome/fontawesome-free/css/all.css";

// needs to be below vuetify
import "./assets/main.scss";
import "./assets/print.scss";
// start loading
import "./globals";

const app = createApp(App);
app.use(VueSortable);
app.use(vuetify);
app.use(router);
app.use(VuetifyNotifier, {
    default: {
        defaultColor: "surface",
        closeIcon: "mdi mdi-close",
        toastProps: {
            location: "top center",
        }
    },
});

app.mount("#app");